Name:		xdragon
Version:	1.1.2
Release:        4
Summary:	Simple drag-and-drop source/sink for X

License:	GPLv3+
URL:		https://gitlab.com/jeremieglbd/xdragon/
Source0:	https://gitlab.com/jeremieglbd/xdragon/-/archive/master/xdragon-master.tar.gz

BuildRequires: gtk3-devel gtk3 gcc
#Requires:

%description

%global debug_package %{nil}

%prep
%setup


%build
make


%install
make PREFIX=%{buildroot}/usr/local/bin/ install

%check


%files
%license
%doc
/usr/local/bin/xdragon

%changelog

