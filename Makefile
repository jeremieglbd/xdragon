PREFIX = $(HOME)/.local/bin

all: xdragon

dragon: xdragon.c
	$(CC) --std=c99 -Wall $(DEFINES) xdragon.c -o xdragon `pkg-config --cflags --libs gtk+-3.0`

install: dragon
	mkdir -p $(PREFIX)
	cp xdragon $(PREFIX)
